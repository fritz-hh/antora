= Install Antora
ifndef::env-site,env-github[]
include::_attributes.adoc[]
endif::[]
:version: 1.0.0

Assumptions:

* [x] You've installed the base build tools on your xref:install/linux-requirements.adoc#base-build-tools[Linux] or xref:install/macos-requirements.adoc#base-build-tools[macOS] machine (not applicable for Windows).
* [x] You've installed Node 8 on your xref:install/linux-requirements.adoc#node-8[Linux], xref:install/windows-requirements.adoc#node-8[Windows], or xref:install/macos-requirements.adoc#node-8[macOS] machine.

On this page, you'll learn:

* [x] How to install the Antora CLI.
* [x] How to install the default Antora site generator.

To generate documentation sites with Antora, you need the Antora command line interface (CLI) and an Antora site generator pipeline.
Once these packages are installed, you can use the `antora` command to publish a site.

== Install the Antora CLI

We'll begin by installing the Antora CLI using the base call for Node's package manager, `npm`.
This package manager was installed when you installed Node.

You should install (`i`) the CLI globally (`-g`) so the `antora` command is available on your PATH.

Open a terminal and type:

 $ npm i -g @antora/cli

IMPORTANT: When we say "`globally`" here, it does not imply system-wide.
It means the location where Node is installed.
If you used nvm to install Node, this location will be inside your home directory (thus not requiring elevated permissions).

NOTE: If you prefer Yarn over npm, you can install Antora using `yarn global add @antora/cli`.

Verify the `antora` command is available on your PATH by running:

 $ antora -v

If the installation worked, this command should report the version of Antora.

[subs=attributes+]
 $ antora -v
 {version}

== Install the default Antora site generator

Next, install the default site generator.

To install the generator globally, type:

 $ npm i -g @antora/site-generator-default

If you don't want to install the generator globally, you can opt to install it inside the playbook project (the project that contains the playbook file(s) for your site).

Switch to the playbook project and type:

 $ npm i @antora/site-generator-default

CAUTION: If you're on Linux and get an error message about `libcurl-gnutls.so.4`, you'll need to xref:install/troubleshoot-nodegit.adoc[patch or recompile nodegit].

== What's next?

Now that the Antora CLI and default site generator are installed, you are ready to:

* Set up your own xref:playbook:index.adoc[playbook] or use the Demo playbook.
* Organize a xref:component-structure.adoc[documentation component repository] or use Antora's Demo docs components.
* xref:run-antora.adoc[Run Antora] and generate a documentation site.
